﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SOAttack))]
public class EditorSOAttack : Editor{

	SOAttack soattack;

	/// <summary>
	/// Añade boton de recargar PPs a los ataques
	/// </summary>
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		if (GUILayout.Button("Recargar PP")) {
			soattack.PP = soattack.maxPP;
		}
	}

	public void OnEnable() {
		soattack = (SOAttack)target;
	}
}
