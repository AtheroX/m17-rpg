﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SOPet))]
public class EditorSOPet : Editor{

	SOPet sopet;

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		if (GUILayout.Button("Regenerar vida")) {
			sopet.hp = sopet.maxHp;
		}
		if (GUILayout.Button("Subir nivel")) {
			sopet.GainExp(sopet.maxXp);
		}
		if (GUILayout.Button("Bajar nivel")) {
			sopet.lvl--;
			sopet.RecalcEXP();
		}
	}

	public void OnEnable() {
		sopet = (SOPet)target;
	}
}
