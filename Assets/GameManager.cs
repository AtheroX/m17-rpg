﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

	static Dictionary<int, int> hudsPorEscenas = new Dictionary<int, int> { { (int)Scenes.NAVE , 3 }, { (int)Scenes.CAFETERÍA, 4 } };

	public GameObject playerGO;
	public SOPlayer data;
	string playerSpawnedFirstTimePlayerPref = "hasPlayerSpawned";
    public Transform HUD;
    bool exitKeyDown = false, exitMenuOpen = false, shopMenuOpen = false , firstPetMenuOpen = false , firstPetTiendaOpen = false;
	int escapeHud;

    void Start() {
		//Si la escena del archivo de guardado difiere del actual, recarga la escena
		if (SceneManager.GetActiveScene().buildIndex != data.sceneToLoad)
			data.ReturnToMyPosition();

		//Si no hay un player en la escena, lo crea (por si acaso)
		if (!FindObjectOfType<CharacterController>())
			SpawnPlayer();

		//Cuenta la cantidad de huds que debería tener cada escena y las desactiva de inicio (no cuenta la pantalla de FADEIN)
		hudsPorEscenas.TryGetValue(SceneManager.GetActiveScene().buildIndex, out int huds);
		for (int i = 0; i < huds; i++) {
			HUD.GetChild(i).gameObject.SetActive(false);
		}

		//Número de posición del hud en el que está el hud de pets e items
		hudsPorEscenas.TryGetValue(SceneManager.GetActiveScene().buildIndex, out int escapeHud);
		this.escapeHud = escapeHud;
	}

	void Update(){
		// Si detecta el escape, no estaba abierto y no hay ninguna tienda abierta abre el escape
        if (Input.GetAxis("Cancel") !=0 && !exitKeyDown && !shopMenuOpen && !firstPetMenuOpen && !firstPetTiendaOpen) {
			HUD.GetChild(0).gameObject.SetActive(!exitMenuOpen);
            HUD.GetChild(escapeHud - 1).gameObject.SetActive(!exitMenuOpen);
            HUD.GetChild(escapeHud - 1).GetChild(0).gameObject.SetActive(false);
            HUD.GetChild(escapeHud - 1).GetChild(1).gameObject.SetActive(false);
            UpdateMoney();
            CharacterController p = playerGO.GetComponent<CharacterController>();
            p.canIMove = !p.canIMove;
            exitMenuOpen = !p.canIMove;
            exitKeyDown = true;
        }
        if (Input.GetAxis("Cancel") == 0 ){          
            exitKeyDown = false;
		}
	}

	/// <summary>
	/// Crea al player y lo reposiciona con la velocidad de rotación que debería ser
	/// </summary>
	public void SpawnPlayer() {
		playerGO = Instantiate(playerGO);
		Rigidbody2D rb = playerGO.GetComponent<Rigidbody2D>();
		rb.position = data.position;
		rb.angularVelocity = data.angularVelocity;
	}

	/// <summary>
	/// Actualiza el texto de dinero del escape
	/// </summary>
	public void UpdateMoney()
    {
		HUD.GetChild(0).GetChild(4).GetComponent<TMPro.TextMeshProUGUI>().SetText("Dinero: "+ data.GetMoneda());
    }

	/// <summary>
	/// Muestra el menu de tienda
	/// </summary>
    public void EnableShopMenu() {
        if (exitMenuOpen)
            return;
        shopMenuOpen = !shopMenuOpen;
        HUD.GetChild(1).gameObject.SetActive(shopMenuOpen);
        playerGO.GetComponent<CharacterController>().canIMove  = !shopMenuOpen;
    }

	/// <summary>
	/// Muestra la tienda de pets y curar
	/// </summary>
	public void EnableShopHealMachineMenu()
    {
        if (exitMenuOpen)
            return;
        firstPetTiendaOpen = !firstPetTiendaOpen;
        HUD.GetChild(2).gameObject.SetActive(firstPetTiendaOpen);
        HUD.GetChild(2).GetChild(0).gameObject.SetActive(false);
        HUD.GetChild(2).GetChild(1).gameObject.SetActive(true);
        playerGO.GetComponent<CharacterController>().canIMove = !firstPetTiendaOpen;
    }

	/// <summary>
	/// Muestra en la nave el elegir primera pet
	/// </summary>
	public void EnableChosePetMenu()
    {
        if (exitMenuOpen)
            return;
        firstPetMenuOpen = !firstPetMenuOpen;
        HUD.GetChild(1).gameObject.SetActive(firstPetMenuOpen);
        HUD.GetChild(1).GetChild(0).gameObject.SetActive(true);
        HUD.GetChild(1).GetChild(1).gameObject.SetActive(false);
        playerGO.GetComponent<CharacterController>().canIMove = !firstPetMenuOpen;
    }

	/// <summary>
	/// Guarda los datos del jugador en un JSON
	/// </summary>
    public void save() {
		data.angularVelocity = playerGO.GetComponent<Rigidbody2D>().angularVelocity;
        data.sceneToLoad = SceneManager.GetActiveScene().buildIndex;
        data.position = playerGO.transform.position;

        string jJonson = JsonUtility.ToJson(data);
        File.WriteAllText("save.json", jJonson);
        print("Guardado con exito");
    }

	/// <summary>
	/// Carga los datos del JSON al jugador
	/// </summary>
    public void load()
    {
        string jJonson = File.ReadAllText("save.json");
        JsonUtility.FromJsonOverwrite(jJonson, data);
        print("Datos cargados con exito");
		data.ReturnToMyPosition();
    }

	/// <summary>
	/// Salir de partida
	/// </summary>
    public void ExitGame() {
        Application.Quit();
    }
}
