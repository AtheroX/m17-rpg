﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rbPJ;
    Vector2 movimiento;
    public SOPlayer data;
    public bool canIMove = true;


    void Update() {
        if (canIMove) {
            movimiento.x = Input.GetAxisRaw("Horizontal");
            movimiento.y = Input.GetAxisRaw("Vertical");

            if (Input.GetAxisRaw("Jump") != 0) {
                if (rbPJ.angularVelocity > 0)
                    rbPJ.angularVelocity += 2f;
                else
                    rbPJ.angularVelocity -= 2f;
            } else 
                transform.Rotate(new Vector3(0, 0, 0));
            
        } else 
            movimiento = Vector2.zero;
        

		if (Input.GetKeyDown(KeyCode.F)) {
			CheckForInteractuables();
		}
        
    }

    private void FixedUpdate(){
        rbPJ.MovePosition(rbPJ.position + movimiento * speed * Time.fixedDeltaTime);
    }

	/// <summary>
	/// Comprueba si en cierto rango hay alguna tienda, maquina de cura o zona de primera Pet
	/// </summary>
    void CheckForInteractuables() {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, .5f);
        Shop tienda = null;        
        FirstPet pet = null;
        ShopPets tiendaPet = null;
		foreach (Collider2D hit in hits) {
			if (tienda = hit.GetComponent<Shop>()) {
				canIMove = false;
				FindObjectOfType<GameManager>().EnableShopMenu();
				tienda.Display();
				break;

			} else if (pet = hit.GetComponent<FirstPet>()) {
				if (data.pets[0] == null) {
					canIMove = false;
					FindObjectOfType<GameManager>().EnableChosePetMenu();
					break;
				}
			} else if (tiendaPet = hit.GetComponent<ShopPets>()) {
				canIMove = false;
				FindObjectOfType<GameManager>().EnableShopHealMachineMenu();
				tiendaPet.Display();
				break;

			}
		}
	}

	/// <summary>
	/// Posiciona al player en el punto guardado
	/// </summary>
	public void respawn() {
		transform.position = data.position;
	}

	/// <summary>
	/// Muestra el rango de acción de las máquinas
	/// </summary>
    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, .5f);
    }

}
