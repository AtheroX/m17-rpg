﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// Script que controla el sistema de combate
/// </summary>
public class CombatSystem : MonoBehaviour
{
    public SOPlayer data;
    public SOGotoCombat combatData;
	SOPet myPet, enPet;
    public GameObject myHud, enHud; 
	public GameObject attacks, attackGO;
	Slider myHpSlider, enHpSlider;
	public GameObject canvasGO;
	public enum CombatMenu { COMBAT, INVENTORY, PETS }
    enum CombatState { MYTURN, ETURN, ACTION }
    private CombatState actualState;
    int atkSelected;
    int enAtkSelected;

	/// <summary>
	/// Inicializa/carga pets, HUD, ataques, CombatState,...
	/// </summary>
    void Awake()
    {
		attacks.SetActive(false);
        myPet = data.ChooseFirstAlive();
        enPet = SOPet.clone(combatData.enPet);

		LoadMyHud();
		LoadEnHud();
		LoadAttacks();
        actualState = CombatState.MYTURN;
		ChangeCombatMenu(CombatMenu.COMBAT);
		InventoryManager(0);
		canvasGO.transform.GetChild(1).GetChild(0).gameObject.GetComponent<ItemDisplayer>().cs = this;
	}

	/// <summary>
	/// Según el enum CombatState ahce una acción
	/// En su turno elige su ataque y en Acción, según la agilidad de las pets se decide quién ataca 
	/// </summary>
    void Combat()
    {
		if (actualState == CombatState.ETURN) //Selecciona aleatorio o con algoritmo el ataque
		{
			enAtkSelected = Random.Range(0, enPet.attacks.Length); //el enemigo coge aleatoriamente uno de sus ataques 
			actualState = CombatState.ACTION;
			Combat();
		} else if (actualState == CombatState.ACTION) {
			if (myPet.agl >= enPet.agl) {
				StartCoroutine(FightAnimation(true));
			} else{
				StartCoroutine(FightAnimation(false));
			}
		}
	}

	/// <summary>
	/// Entra un bool y si es true inicia corrutina y atacas tú primero, si no, 
	/// ataca el enemigo
	/// </summary>
	/// <param name="iHitted">Determina que he sido yo quien ha empezado</param>
    IEnumerator FightAnimation(bool iHitted)
    {
        int finalDmg;
        if (iHitted)
        {
			print("Fightanimation MIO");
            finalDmg = MyDmgCalc();
            StartCoroutine(Hit(enPet, finalDmg, iHitted));
            
            yield return new WaitForSeconds(1.5f);
            finalDmg = EnDmgCalc();
            StartCoroutine(Hit(myPet, finalDmg, !iHitted));            
        }
        else {
			print("Fightanimation SUYO");
			finalDmg = EnDmgCalc();
            StartCoroutine(Hit(myPet, finalDmg, iHitted));
            
            yield return new WaitForSeconds(1.5f);
            finalDmg = MyDmgCalc();
            StartCoroutine(Hit(enPet, finalDmg, !iHitted));
        }
    }

	/// <summary>
	/// Hace daño a la pet que se le pasa y actualiza su barra de vida
	/// </summary>
	/// <param name="pet"></param>
	/// <param name="dmg"></param>
	/// <param name="ihitted"></param>
    IEnumerator Hit(SOPet pet, int dmg, bool ihitted) 
	{
		print("Daño: "+dmg +" a la pet:"+pet.name+" con "+ihitted);
		if (ihitted) {
            int anterior = enPet.hp;
            enPet.GetHit(dmg);
            for (float t = 0; t <= 1f; t += Time.deltaTime) {
                enHpSlider.value = Mathf.Lerp(anterior, enPet.hp, t);
                yield return null;
            }
			if (enPet.hp <= 0) {
				combatData.CombatEnded(true);
			}
		} else {
            int anterior = myPet.hp;
            myPet.GetHit(dmg);
            for (float t = 0; t <= 1f; t += Time.deltaTime) {
                myHpSlider.value = Mathf.Lerp(anterior, myPet.hp, t);
                yield return null;
            }
			if (myPet.hp <= 0) {
				myPet = data.ChooseFirstAlive();
				if (myPet == null)
					combatData.CombatEnded(false);
				else
					ChangePet(myPet);
			}
		}
	}

	/// <summary>
	/// Dañar a la pet elegida con el daño pasado
	/// </summary>
	/// <param name="pet"></param>
	/// <param name="dmg"></param>
	/// <param name="iHitted"></param>
    public void DamageWithItem(SOPet pet, int dmg, bool iHitted) 
	{
        StartCoroutine(Hit(pet, dmg, iHitted));
	}

	/// <summary>
	/// Calcula el daño de tu pet según si el tipo es favorable, nivel, defensa,...
	/// </summary>
	/// <returns>El daño final a hacer</returns>
	private int MyDmgCalc() 
	{
		print("CALCULANDO MI DAMAGE");
		attacks.transform.GetChild(atkSelected).GetComponent<Button>().interactable = !myPet.attacks[atkSelected].UseAttack();
		attacks.transform.GetChild(atkSelected).GetChild(1).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>()
			.SetText(myPet.attacks[atkSelected].PP + "/" + myPet.attacks[atkSelected].maxPP);

		float coso = Random.value;
		print(coso + " | " + myPet.attacks[atkSelected].precision);
		if (coso > myPet.attacks[atkSelected].precision) {
			print("ATAQUE FALLADO");
			return 0;
		}

		float typePet = new CompareTypes().Compare(myPet.attacks[atkSelected].type, enPet.type);

		if (typePet == 1)
			typePet = 1.5f;
		else if (typePet == -1)
			typePet = 0.75f;
		else
			typePet = 1;

		float bonificadorDeDmg = Random.Range(85, 101);
		float lvlvalue = (0.2f * (myPet.lvl + 1) * myPet.attacks[atkSelected].dmg);
		float defvalue = enPet.def;

		print(typePet + " | " + bonificadorDeDmg + " | " + lvlvalue + " | " + defvalue + " \\ " + (lvlvalue / defvalue));

		float calc = typePet * bonificadorDeDmg * (lvlvalue * 1f / defvalue * 1f) / 100;
		return (int)calc;
	}

	/// <summary>
	/// Calcula el daño de la pet enemiga según si el tipo es favorable, nivel, defensa,...
	/// </summary>
	/// <returns>El daño final a hacer</returns>
	private int EnDmgCalc() 
	{
		print("CALCULANDO SU DAMAGE");
		float coso = Random.value;
		print(coso + " | " + enPet.attacks[enAtkSelected].precision);
		if (coso > enPet.attacks[enAtkSelected].precision) {
			print("ATAQUE FALLADO");
			return 0;
		}

		float typePet = new CompareTypes().Compare(enPet.attacks[enAtkSelected].type, myPet.type);
        
        if (typePet == 1)
            typePet = 2;
        else if (typePet == -1)
            typePet = 0.5f;
        else
            typePet = 1;

        float bonificadorDeDmg = Random.Range(85, 101);
		float lvlvalue = (0.2f * (enPet.lvl + 1) * enPet.attacks[enAtkSelected].dmg);
		float defvalue = myPet.def;

		float calc = (typePet * bonificadorDeDmg * (lvlvalue * 1f / defvalue * 1f) / 100);
		return (int)calc;
	}

	/// <summary>
	/// Carga tu HUD, los sliders, nombre, nivel y eso
	/// </summary>
    private void LoadMyHud() 
	{
        myHud.transform.parent.GetChild(2).GetChild(0).GetChild(0).GetComponent<Image>().sprite = myPet.sprite;
        myHud.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.name.ToUpperInvariant());
		myHud.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.lvl + "");
		myHpSlider = myHud.transform.GetChild(1).GetChild(1).GetComponent<Slider>();
		//myHpSlider.wholeNumbers = true;
		myHpSlider.maxValue = myPet.maxHp;
		myHpSlider.value = myPet.hp;

		Slider xp = myHud.transform.GetChild(2).GetChild(0).GetComponent<Slider>();
		xp.wholeNumbers = true;
		xp.maxValue = myPet.maxXp;
		xp.value = myPet.xp;
	}

	/// <summary>
	/// Carga el HUD enemigo, los sliders, nombre, nivel y eso
	/// </summary>
	private void LoadEnHud()
    {
        enHud.transform.parent.GetChild(3).GetChild(0).GetChild(0).GetComponent<Image>().sprite = enPet.sprite;
        enHud.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(enPet.name.ToUpperInvariant());
		enHud.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(enPet.lvl + "");
		enHpSlider = enHud.transform.GetChild(1).GetChild(1).GetComponent<Slider>();
		//enHpSlider.wholeNumbers = true;
		enHpSlider.maxValue = enPet.maxHp;
		enHpSlider.value = enPet.hp;
	}

	/// <summary>
	/// Crea los botones de tus ataques
	/// </summary>
	void LoadAttacks() 
	{
		for (int i = 0; i < myPet.attacks.Length; i++) {
			if (myPet.attacks[i] == null)
				break;
			GameObject o = Instantiate(attackGO, attacks.transform);
			o.SetActive(true);
			o.transform.name = "Attack" + i;
			o.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].name);
			o.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].PP + "/" + myPet.attacks[i].maxPP);
			o.transform.GetChild(1).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].type.ToString().ToUpper());
			Button b = o.GetComponent<Button>();
			b.interactable = myPet.attacks[i].CanBeUsed();
			int num = i;
			b.onClick.AddListener(() => { SelAtk(num); });
		}
	}

	/// <summary>
	/// Seleccionar mi ataque
	/// </summary>
	/// <param name="i">Numero de ataque </param>
	void SelAtk(int i)
    {
		atkSelected = i;
		attacks.SetActive(false);
		actualState = CombatState.ETURN;
		Combat();
	}

	/// <summary>
	/// Cambia el menú de combate según el enum CombatEnum
	/// </summary>
	/// <param name="cm">Menú a mostrar</param>
	public void ChangeCombatMenu(CombatMenu cm) 
	{
		for (int i = 0; i < Enum.GetValues(typeof(CombatMenu)).Length; i++) {
			if ((int)cm == i)
				canvasGO.transform.GetChild(i).gameObject.SetActive(true);
			else
				canvasGO.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Cambia el menú según el combat menú
	/// </summary>
	/// <param name="cm"></param>
	public void ChangeCombatMenu(int cm) 
	{
		for (int i = 0; i < Enum.GetValues(typeof(CombatMenu)).Length; i++) {
			if (cm == i)
				canvasGO.transform.GetChild(i).gameObject.SetActive(true);
			else
				canvasGO.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Cambia pet
	/// </summary>
	/// <param name="selectedPet"></param>
	public void ChangePet(SOPet selectedPet)
    {
		print("select:" + selectedPet);
		myPet = selectedPet;
		LoadMyHud();
		for (int i = 0; i < myPet.attacks.Length; i++) {
			Destroy(attacks.transform.GetChild(i).gameObject);
		}
		LoadAttacks();
	}

	/// <summary>
	/// Cambia dentro del menú de inventory si se ven Pets o items
	/// </summary>
	/// <param name="i">0= Items - 1= Pets</param>
	public void InventoryManager(int i) 
	{
		Transform inventory = canvasGO.transform.GetChild(1);
		if(i == 0) {
			inventory.GetChild(0).gameObject.SetActive(true);
			inventory.GetChild(1).gameObject.SetActive(false);			
		}else{
			inventory.GetChild(0).gameObject.SetActive(false);
			inventory.GetChild(1).gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// Retorna la pet enemiga
	/// </summary>
	/// <returns></returns>
    public SOPet GetEnemyPet() 
	{
        return enPet;
    }
}
