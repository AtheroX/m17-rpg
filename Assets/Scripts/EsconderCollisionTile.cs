﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EsconderCollisionTile : MonoBehaviour{

    private TilemapRenderer tilemapRenderer;

    void Start(){
        tilemapRenderer = GetComponent<TilemapRenderer>();
        tilemapRenderer.enabled = false;        
    }

} 