﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstPet : MonoBehaviour{

    public SOPet[] petArrary;
    GameObject hudpet;
    GameObject hudAccept;
    public GameObject petGO;
    public SOPlayer data;
    SOPet selectedPet;

    void Start() { 
        hudpet = FindObjectOfType<GameManager>().HUD.transform.GetChild(1).GetChild(0).gameObject;
        hudAccept = FindObjectOfType<GameManager>().HUD.transform.GetChild(1).GetChild(1).gameObject;

        hudAccept.SetActive(false);
        if (petArrary.Length < 1)
            return;

        // Se itera la array de pet, se crean los objetos boton de cada pet y se le asigna la informacion adecuada
        for (int i = 0; i < petArrary.Length; i++)
        {
            if (petArrary[i] == null)
                break;

            GameObject pet = Instantiate(petGO, hudpet.transform);
            
            pet.transform.name = petArrary[i].name;
            pet.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(petArrary[i].name + " Lvl" + petArrary[i].lvl);
            pet.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("" + petArrary[i].type);
            pet.transform.GetChild(2).GetComponent<Image>().sprite = petArrary[i].sprite;
            TMPro.TextMeshProUGUI valores = pet.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

            valores.SetText(valores.text + "\n HP " + petArrary[i].hp + " / " + petArrary[i].maxHp);
            valores.SetText(valores.text + "\n DMG " + petArrary[i].dmg);
            valores.SetText(valores.text + "\n AGL " + petArrary[i].agl);
            valores.SetText(valores.text + "\n DEF " + petArrary[i].def);

			/*	Por alguna razón si intentas pasarle la i a los listeners estos explotan y ponen el valor final de i del bucle
				En caso de un bucle de 3 pues a todos los listeners le envia un 3 */ 
            int val = i;
           
            // Cuando clicas a la pet se guarda en la variable selected pet y se activa el menú de aceptar
            pet.GetComponent<Button>().onClick.AddListener(() =>
            {
                hudpet.SetActive(!hudpet.activeSelf);
                hudAccept.SetActive(!hudAccept.activeSelf);
                selectedPet = petArrary[val];
            });  
        }

        // Botón de aceptar para guardar la pet en la array del player
        hudAccept.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() =>
        {
            data.SavePet(selectedPet);
            FindObjectOfType<GameManager>().EnableChosePetMenu();

        });
    }  
}
