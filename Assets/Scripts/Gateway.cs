﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gateway : MonoBehaviour
{
    public int id;
    private Vector2 outposition;
    static Dictionary<string, Vector2> gatePositions = new Dictionary<string, Vector2>();
    public SOPlayer data;
    public Scenes OutSceneId;
    public int OutGatewayId;

    void Awake() {
		string a = SceneManager.GetActiveScene().buildIndex + "," + id;
		if (gatePositions.ContainsKey(a))
			return;

		outposition = transform.GetChild(0).position;
        gatePositions.Add(a, outposition);
    }

	/// <summary>
	/// Si no tiene el update no se puede habilitarse y deshabilitarse
	/// </summary>
    void Update(){
        
    }

	/// <summary>
	/// Al entrar en la puerta se detecta y empieza a cargar
	/// </summary>
	/// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision){
		if(collision.transform.tag == "Player" && enabled)
			StartCoroutine(LoadAsync(collision.gameObject));
	}

	/// <summary>
	/// Empieza a cargar asyncronamente la escena a la que debe ir y cuando está a punto de acabar, guarda y se mueve a la otra escena.
	/// Al terminar de cargar permite el cambio de escena
	/// </summary>
	/// <param name="player"></param>
	/// <returns></returns>
	IEnumerator LoadAsync(GameObject player) {
		AsyncOperation async = SceneManager.LoadSceneAsync((int)OutSceneId);
		async.allowSceneActivation = false;
		while (!async.isDone) {
			print("Loading async... " + async.progress);
			if (async.progress >= 0.9f) {
				data.angularVelocity = player.GetComponent<Rigidbody2D>().angularVelocity;
				data.sceneToLoad = (int)OutSceneId;
				async.completed += (s) => {
					data.position = gatePositions[(int)OutSceneId + "," + OutGatewayId];
				};
			}
			async.allowSceneActivation = true;
			yield return null;
		}		
	}
}
public enum Scenes { NAVE = 0, CAFETERÍA = 1 }