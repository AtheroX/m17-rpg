﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GrassSpawner : MonoBehaviour{

    [System.Serializable]
    public class GrassPet {
        public SOPet pet;
        [Tooltip("Cuanta frecuencia tendrá, a mayor más sale")] public int veces;
    }

    GameManager gm;
    public SOPlayer data;
    public SOGotoCombat combatData;
    public GrassPet[] pool;
    public bool inGrass = false;
    public Vector2 lastPos;
    public int minPasos, maxPasos;
    int pasos;

    List<int> probabilidades = new List<int>();

    void Start() {
        gm = FindObjectOfType<GameManager>();
    }

	/// <summary>
	/// Si entra en cesped va a la corrutina de estár en cesped
	/// </summary>
	/// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.transform.tag == "Player") {
            inGrass = true;
			if (data.ChooseFirstAlive() == null)
				return;
            StartCoroutine(InGrass());
        }
    }

	/// <summary>
	/// Si sale del cesped se reinician los pasos
	/// </summary>
	/// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.transform.tag == "Player") {
            inGrass = false;
            pasos = 0;
        }
    }

	/// <summary>
	/// A cada metro andado se incrementa un paso y se comprueba si los pasos actuales son más que el mínimo
	/// Si supera el mínimo, se hace un porcentaje de probabilidad de aparición de enemigos 
	/// usando el mínimo de pasos y el máximo (donde es 100% de probabilidades de aparición).
	/// 
	/// En caso de que tenga que aparecer uno, se pillan todos los que pueden aparecer y sus probabilidades, 
	/// se meten en una lista y como si fuese sacar una bola de la lotería, saca el Pet a combatir. 
	/// 
	/// Tras esto, se guardan en un SOGotoCombat y se carga la escena combate.
	/// 
	/// En caso de que no haya aparecido un combate, espera 0.2 y vuelve a hacer toda la comprobación (Es mentira, no funciona aún)
	/// </summary>
	IEnumerator InGrass() {
        if (Vector2.Distance(gm.playerGO.transform.position, lastPos) > 1f) {
            pasos++;
            lastPos = gm.playerGO.transform.position;

            if (pasos > minPasos) {
                float porcentaje = ( pasos - minPasos)*100 / ( maxPasos - minPasos );
                if(Random.value*100 < porcentaje) {
                    for (int i = 0; i < pool.Length; i++) {
                        for (int j = 0; j < pool[j].veces; j++) {
                            probabilidades.Add(i);
                        }
                    }
                    GoToCombat();
                }
            }
            //if()
        }
        yield return new WaitForSeconds(0.2f);
        if(inGrass)
            StartCoroutine(InGrass());
    }

	/// <summary>
	/// Elige la pet que debe salir a combatir, guarda la posición actual del jugador y carga la escena de combate.
	/// </summary>
    void GoToCombat() {
        combatData.enPet = pool[probabilidades[Random.Range(0, probabilidades.Count)]].pet;
        data.position = gm.playerGO.transform.position;
        SceneManager.LoadScene("Combate");
    }
}
