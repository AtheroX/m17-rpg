﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tiene 2 apartados principales, en caso de ser utilizado para peleas, utiliza el OnEnable
/// para que cada vez que se abra el ItemDisplayer se actualice la lista, en caso de no ser para 
/// peleas, significa que será un menú y los items no actuan y tampoco en necesaria actualizarse,
/// con lo cual lo hace en el start.
/// </summary>
public class ItemDisplayer : MonoBehaviour
{    
    public SOPlayer soPj;
    public GameObject guiItem;
	[HideInInspector] public CombatSystem cs;

	public bool onCombat = true;

	SOItem seleccionado;

	/// <summary>
	/// Crea objetos botón para cada item con su información. (Fuera de combate)
	/// </summary>
	void Start() {
		if (onCombat)
			return;

		cs = FindObjectOfType<CombatSystem>();

		for (int i = 0; i < soPj.items.Count; i++) {
			if (soPj.items[i].quantity < 1)
				continue;
			GameObject item = Instantiate(guiItem, transform.GetChild(0));
			item.transform.name = soPj.items[i].name;
			item.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(soPj.items[i].name);
			item.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("×" + soPj.items[i].quantity);
			item.transform.GetChild(2).GetComponent<Image>().sprite = soPj.items[i].sprite;
			TMPro.TextMeshProUGUI valores = item.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

			if (soPj.items[i].showStats) {
				if (soPj.items[i].extraHP != 0) {
					valores.SetText(valores.text + "\n HP " + soPj.items[i].extraHP);
				}
				if (soPj.items[i].extraAGL != 0) {
					valores.SetText(valores.text + "\n AGL " + soPj.items[i].extraAGL);
				}
				if (soPj.items[i].extraDEF != 0) {
					valores.SetText(valores.text + "\n DEF " + soPj.items[i].extraDEF);
				}
				if (soPj.items[i].extraDMG != 0) {
					valores.SetText(valores.text + "\n DMG " + soPj.items[i].extraDMG);
				}
			}

			item.transform.GetChild(4).GetComponent<TMPro.TextMeshProUGUI>().SetText(soPj.items[i].descripcion);
			
		}
	}

	/// <summary>
	/// Crea objetos botón para cada item con su información. (En combate)
	/// </summary>
	void OnEnable(){
		if (!onCombat)
			return;

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        // Itera la lista de items que tiene el pj y le asigna la informacion a cada campo
        for (int i = 0; i < soPj.items.Count; i++) {
			if (soPj.items[i].quantity < 1)
				continue;
            GameObject item = Instantiate(guiItem, transform);
            item.transform.name = soPj.items[i].name;
            item.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(soPj.items[i].name);
            item.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("×" + soPj.items[i].quantity);
            item.transform.GetChild(2).GetComponent<Image>().sprite = soPj.items[i].sprite;
            TMPro.TextMeshProUGUI valores = item.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

            if (soPj.items[i].showStats) {
                if (soPj.items[i].extraHP != 0) {
                    valores.SetText(valores.text + "\n HP " + soPj.items[i].extraHP);
                }
                if (soPj.items[i].extraAGL != 0) {
                    valores.SetText(valores.text + "\n AGL " + soPj.items[i].extraAGL);
                }
                if (soPj.items[i].extraDEF != 0) {
                    valores.SetText(valores.text + "\n DEF " + soPj.items[i].extraDEF);
                }
                if (soPj.items[i].extraDMG != 0) {
                    valores.SetText(valores.text + "\n DMG " + soPj.items[i].extraDMG);
                }
            }

            item.transform.GetChild(4).GetComponent<TMPro.TextMeshProUGUI>().SetText(soPj.items[i].descripcion);
			
			/*	Por alguna razón si intentas pasarle la i a los listeners estos explotan y ponen el valor final de i del bucle
				En caso de un bucle de 3 pues a todos los listeners le envia un 3 */
			int val = i;
			item.GetComponent<Button>().onClick.AddListener(() => {
                seleccionado = soPj.items[val];
                if (!seleccionado.forTheEnemy)
                    cs.InventoryManager(1);
                else {
                    UseItem(cs.GetEnemyPet(), true);
                    cs.ChangeCombatMenu(CombatSystem.CombatMenu.COMBAT);
                }
			});
        }

    }

    /// <summary>
    /// Utiliza el item en la pet que recibe
    /// </summary>    
	public void UseItem(SOPet pet, bool doesDamage = false) {
		if (seleccionado == null)
			return;

        if(doesDamage)
            seleccionado.UseItem(pet, cs);
        else
            seleccionado.UseItem(pet);
		seleccionado = null;
	}
}
