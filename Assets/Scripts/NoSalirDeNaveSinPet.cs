﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoSalirDeNaveSinPet : MonoBehaviour
{
    public SOPlayer data;

    void Start(){
        GetComponent<Gateway>().enabled = false;
    }

	/// <summary>
	/// Si intentas entrar por la puerta y no tienes pet, te comes los mocos.
	/// </summary>
	/// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag == "Player" && data.ChooseFirstAlive() != null) {
            GetComponent<Gateway>().enabled = true;
            this.enabled = false;
        }
    }

}
