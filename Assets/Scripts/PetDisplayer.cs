﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PetDisplayer : MonoBehaviour {

	public SOPlayer soPj;
	public GameObject guiItem;

	public bool changePet = true, ignoreButtons = false;
	[HideInInspector] public CombatSystem cs;

	void Start() {
		cs = FindObjectOfType<CombatSystem>();
	}

	void OnEnable() {
		for (int i = 0; i < transform.childCount; i++) {
			Destroy(transform.GetChild(i).gameObject);
		}
        if (soPj.pets.Count < 1)
            return;
        // itero la lista de pets del pj y le asigno la informacion a cada campo
        for (int i = 0; i < soPj.pets.Count; i++) {
            if (soPj.pets[i] == null)          
                break;
          
			GameObject pet = Instantiate(guiItem, transform);

			pet.transform.name = soPj.pets[i].name;
			pet.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(soPj.pets[i].name + " Lvl" + soPj.pets[i].lvl);
			pet.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("" + soPj.pets[i].type);
			pet.transform.GetChild(2).GetComponent<Image>().sprite = soPj.pets[i].sprite;
			TMPro.TextMeshProUGUI valores = pet.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

			valores.SetText(valores.text + "\n HP " + soPj.pets[i].hp + " / " + soPj.pets[i].maxHp);
			valores.SetText(valores.text + "\n DMG " + soPj.pets[i].dmg);
			valores.SetText(valores.text + "\n AGL " + soPj.pets[i].agl);
			valores.SetText(valores.text + "\n DEF " + soPj.pets[i].def);


			SOPet selectedPet = soPj.pets[i];
			if (ignoreButtons)
				continue;

            Button b = pet.GetComponent<Button>();
            if (changePet) {
                if (selectedPet.hp == 0)
                    b.interactable = false;
                b.onClick.AddListener(() => {
                    //cambia la pet en el combate
					cs.ChangePet(selectedPet);
					cs.InventoryManager(0);
					cs.ChangeCombatMenu(CombatSystem.CombatMenu.COMBAT);
				});
			} else {
                b.onClick.AddListener(() => {
					transform.parent.GetChild(0).GetComponent<ItemDisplayer>().UseItem(selectedPet);
					cs.InventoryManager(0);
					cs.ChangeCombatMenu(CombatSystem.CombatMenu.COMBAT);
				});
			}
		}
	}
}
