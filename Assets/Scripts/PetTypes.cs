﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PetTypes {

	Phantom, Impostor, Crewmate
	
}
public class CompareTypes : Comparer<PetTypes> {

	public override int Compare(PetTypes x, PetTypes y) {
		if (x == PetTypes.Crewmate && y == PetTypes.Impostor)
			return -1;
		if (y == PetTypes.Crewmate && x == PetTypes.Impostor)
			return 1;
		if (x == PetTypes.Phantom && y == PetTypes.Crewmate)
			return -1;
		if (y == PetTypes.Phantom && x == PetTypes.Crewmate)
			return 1;
		if (x == PetTypes.Impostor && y == PetTypes.Phantom)
			return -1;
		if (y == PetTypes.Impostor && x == PetTypes.Phantom)
			return 1;


		return 0;
	}
}