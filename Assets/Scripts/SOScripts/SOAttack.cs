﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Attack", menuName = "new Attack", order = 2)]
public class SOAttack : ScriptableObject { 
    public new string name;
    public int maxPP;
    public int PP;
    public int dmg;
	public PetTypes type;
	public float precision;

	/// <summary>
	/// Gasta un PP del ataque
	/// </summary>
	/// <returns>Devuelve si le quedan PPs para usarlo</returns>
	public bool UseAttack() {
		PP--;
		if (PP <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/// <summary>
	/// Comprueba si le quedan PPs
	/// </summary>
	/// <returns>Si le quedan PPs devuelve true</returns>
	public bool CanBeUsed() {
		return PP > 0;
	}

}
