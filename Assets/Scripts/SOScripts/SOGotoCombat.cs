﻿using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "new GotoCombat", menuName = "new GotoCombat", order = 5)]
public class SOGotoCombat : ScriptableObject{

    public SOPet enPet;
    public SOPlayer soPlayer;

	/// <summary>
	/// Si ganas da exp a las pets vivas, sino no
	/// </summary>
	/// <param name="ganado"></param>
    public void CombatEnded(bool ganado) {
        if (ganado) {
			foreach (SOPet pet in soPlayer.pets) {
				if (pet == null)
					break;

				if (pet.hp == 0)
					continue;
				soPlayer.SpendMoneda(-10);
				pet.GainExp(15);
			}
		}
		soPlayer.ReturnToMyPosition();
    }
}
