﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Item", menuName = "new Item", order = 4)]

public class SOItem : ScriptableObject{
    public string name;
    public Sprite sprite;
    public int quantity;
    public int extraHP;
    public int extraDMG;
    public int extraAGL;
    public int extraDEF;
    public string descripcion;
    public bool showStats = true;
    public bool forTheEnemy;

	/// <summary>
	/// Utiliza el item en la pet a la que le pases sumandole los extras en los campos que le tocan
	/// </summary>
	/// <param name="pet">Pet a la que efectuar el item</param>
    public void UseItem(SOPet pet) {
        pet.hp = Mathf.Clamp(pet.hp + extraHP, 0, pet.maxHp);
        pet.dmg += extraDMG;
        pet.agl += extraAGL;
        pet.def += extraDEF;
        quantity--;
    }

	/// <summary>
	/// Igual que el anterior pero en caso de que el bool sea true va a una función del Combat system donde hace que
	/// se actualicen las barras de vida en base al daño que hace
	/// </summary>
	/// <param name="pet">Pet a la que efectuar el item</param>
	/// <param name="cs">CombatSystem</param>
	/// <param name="iHitted">Parametro que determina si yo le he dañado o me daña a mi</param>
	public void UseItem(SOPet pet, CombatSystem cs, bool iHitted = true) {
        if (extraHP < 0) 
            cs.DamageWithItem(pet, extraHP * -1, iHitted);
        else
            pet.hp = Mathf.Clamp(pet.hp + extraHP, 0, pet.maxHp);
        pet.dmg += extraDMG;
        pet.agl += extraAGL;
        pet.def += extraDEF;
        quantity--;

    }

}
