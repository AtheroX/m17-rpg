﻿using UnityEngine;

[CreateAssetMenu(fileName ="new Pet", menuName ="new Pet", order = 1)]
public class SOPet : ScriptableObject{
    public string name;
	public Sprite sprite;

    public int maxHp;
    public int hp;

	public int dmg;
	public int agl;
	public int def;
	public PetTypes type;

    public SOAttack[] attacks = new SOAttack[4];

	public int maxXp;
	public int xp;
	public int lvl;

	
	/// <summary>
	/// Daña a la Pet
	/// </summary>
	/// <param name="dmg">Daño que entra</param>
	public void GetHit(int dmg) {
		Debug.Log("Entra:"+dmg+" y tenía:"+hp+" de vida");
		hp = Mathf.Clamp(hp - dmg, 0, maxHp);
	}

	/// <summary>
	/// Gana experiéncia y comprueba si ha subido de nivel
	/// </summary>
	/// <param name="xp">Experiéncia a subir</param>
	public void GainExp(int xp) {
		this.xp += xp;
		LevelUp();
	}

	/// <summary>
	/// Calcula si debe subir de nivel, en caso de subir, resta esa XP, sube nivel y calcula cuanto debe ser el siguiente
	/// </summary>
	void LevelUp() {
		if (xp >= maxXp) {
			xp -= maxXp;
			lvl++;
			RecalcEXP();

			dmg = (int)(dmg * 1.1f);
			agl = (int)(agl * 1.1f);
			def = (int)(def * 1.1f);
			maxHp = (int)(maxHp * 1.1f);
		}
	}

	/// <summary>
	/// Únicamente recalcula cuanta XP necesita para subir de nivel
	/// </summary>
	public void RecalcEXP() {
		maxXp = Mathf.RoundToInt(3 * Mathf.Pow(lvl + 1, 3)) / 5;
	}

	/// <summary>
	/// Clona la pet y devuelve otra igual pero curada
	/// </summary>
	/// <param name="original">Pet a clonar</param>
	/// <returns>Pet clonada y curada</returns>
    public static SOPet clone(SOPet original) {
        SOPet clon = new SOPet();
        clon.agl = original.agl;
        clon.attacks = original.attacks;
        clon.def = original.def;
        clon.dmg = original.dmg;
        clon.name = original.name;
        clon.sprite = original.sprite;
        clon.maxHp = original.maxHp;
        clon.hp = original.maxHp;
        clon.type = original.type;
        clon.lvl = original.lvl;

        return clon;
    }

	/// <summary>
	/// Cura completamente la pet
	/// </summary>
    public void FullHeal(){
        hp = maxHp;
    }
}
