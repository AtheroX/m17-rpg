﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "new PJ", menuName = "new PJ", order = 3)]
public class SOPlayer : ScriptableObject{
    public List<SOPet> pets = new List<SOPet>(6);
    public List<SOItem> items = new List<SOItem>();
    public Vector2 position;
	public int sceneToLoad;
	public float angularVelocity;

	SOItem moneda;
	
	/// <summary>
	/// En caso de no tener la variable de moneda guardada, la busca en el inventario
	/// </summary>
	/// <returns>Devuelve la cantidad de monedas</returns>
	public int GetMoneda() {
		if (moneda == null) {
			bool noerror = false;
			foreach(SOItem i in items) {
				if (i.name.Equals("Moneda")) {
					moneda = i;
					noerror = true;
				}
			}
			if (!noerror)
				return 0;
		}
		return moneda.quantity;
	}

	/// <summary>
	/// Primero comprueba si tiene monedas y tras eso le resta la cantidad que gastes
	/// </summary>
	/// <param name="moneda">Monedas a gastar</param>
    public void SpendMoneda(int moneda){
        if(this.moneda == null)        
            GetMoneda();

        this.moneda.quantity -= moneda;
    }

	/// <summary>
	/// Elige la primera pet viva, si no hay devuelve null
	/// </summary>
	/// <returns>Primera pet disponible, null en caso de todas muertas</returns>
    public SOPet ChooseFirstAlive() {
        foreach (SOPet p in pets) {
            if (p == null)
                break;
            if (p.hp > 0)
                return p;
        }
        return null;
    }

	/// <summary>
	/// Guarda la pet que le pases en el primer hueco disponible
	/// </summary>
	/// <param name="pet">True que ha sido guardada, false si no cabía</param>
    public bool SavePet(SOPet pet) {
        for (int i = 0; i < pets.Count; i++)
        {
            if (pets[i] == null)
            {
                pet.FullHeal();
                pets[i] = pet;
                return true;
            }
        }
		return false;
    }

	/// <summary>
	/// Carga la última escena en la que estuvo
	/// </summary>
    public void ReturnToMyPosition() {
		SceneManager.LoadScene(sceneToLoad);
    }
}
