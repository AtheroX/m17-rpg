﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public SOPlayer soPj;
    GameObject hudShop;
    public GameObject itemGO;

    [System.Serializable]
    public class ItemTienda
    {
        public SOItem item;
        public int price;
    }

    public ItemTienda[] items;

	void Start() {
		hudShop = FindObjectOfType<GameManager>().HUD.transform.GetChild(1).GetChild(0).gameObject;
	}

	public void Display() {
		for (int i = 0; i < hudShop.transform.childCount; i++) {
			Destroy(hudShop.transform.GetChild(i).gameObject);
		}
		if (items.Length < 1)
			return;

        //itera la lista de items y le pone la informacion en cada sitio correspondiente
		for (int i = 0; i < items.Length; i++) {
			if (items[i] == null)
				break;

			GameObject item = Instantiate(itemGO, hudShop.transform);

			item.transform.name = soPj.items[i].name;
			item.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(items[i].item.name);
			item.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("$" + items[i].price);
			item.transform.GetChild(2).GetComponent<Image>().sprite = items[i].item.sprite;
			TMPro.TextMeshProUGUI valores = item.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

			if (items[i].item.extraHP != 0) {
				valores.SetText(valores.text + "\n HP " + items[i].item.extraHP);
			}
			if (items[i].item.extraAGL != 0) {
				valores.SetText(valores.text + "\n AGL " + items[i].item.extraAGL);
			}
			if (items[i].item.extraDEF != 0) {
				valores.SetText(valores.text + "\n DEF " + items[i].item.extraDEF);
			}
			if (items[i].item.extraDMG != 0) {
				valores.SetText(valores.text + "\n DMG " + items[i].item.extraDMG);
			}

			item.transform.GetChild(4).GetComponent<TMPro.TextMeshProUGUI>().SetText(items[i].item.descripcion);
			
			/*	Por alguna razón si intentas pasarle la i a los listeners estos explotan y ponen el valor final de i del bucle
				En caso de un bucle de 3 pues a todos los listeners le envia un 3 */
			int val = i;
			item.GetComponent<Button>().onClick.AddListener(() => {
				comprar(val);
			});
		}
	}
    /// <summary>
    /// la funcion comprar recive in valor, le suma 1 al item en funcion del valor que se le pasa y llama a la funcion gastar moneda
    /// </summary>    
	private void comprar(int val) {
		items[val].item.quantity++;
		soPj.SpendMoneda(items[val].price);
	}
}
