﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPets : MonoBehaviour{

	GameManager gm;
    public SOPlayer soPj;
    GameObject hudPets;
    GameObject hudCurarComprar;
    public GameObject petGO;

	[System.Serializable]
	public class PetTienda {
		public SOPet Pet;
		public int price;
	}
	public PetTienda[] petArrary;
   

    public void Display()
    {
		gm = FindObjectOfType<GameManager>();

		hudPets = gm.HUD.transform.GetChild(2).GetChild(0).gameObject;
        hudCurarComprar = gm.HUD.transform.GetChild(2).GetChild(1).gameObject;

        if (petArrary.Length < 1)
            return;

        for (int i = 0; i < hudPets.transform.childCount; i++)
        {
            Destroy(hudPets.transform.GetChild(i).gameObject);
        }

        // Itera la array de pets para asignarle la info
        for (int i = 0; i < petArrary.Length; i++)
        {
            if (petArrary[i] == null)
                break;

            GameObject pet = Instantiate(petGO, hudPets.transform);

            pet.transform.name = petArrary[i].Pet.name;
            pet.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(petArrary[i].Pet.name + " Lvl" + petArrary[i].Pet.lvl);
            pet.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("" + petArrary[i].Pet.type);
            pet.transform.GetChild(2).GetComponent<Image>().sprite = petArrary[i].Pet.sprite;
            TMPro.TextMeshProUGUI valores = pet.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

            valores.SetText(valores.text + "\n HP "  + petArrary[i].Pet.maxHp);
            valores.SetText(valores.text + "\n DMG " + petArrary[i].Pet.dmg);
            valores.SetText(valores.text + "\n AGL " + petArrary[i].Pet.agl);
            valores.SetText(valores.text + "\n DEF " + petArrary[i].Pet.def);
            
            pet.transform.GetChild(4).GetComponent<TMPro.TextMeshProUGUI>().SetText("$" + petArrary[i].price);

			/*	Por alguna razón si intentas pasarle la i a los listeners estos explotan y ponen el valor final de i del bucle
				En caso de un bucle de 3 pues a todos los listeners le envia un 3 */
			int val = i;

			// Compra esta pet y si tiene espacio para guardar gasta el dinero, sino no
            pet.GetComponent<Button>().onClick.AddListener(() =>
            {               
                bool guardar = soPj.SavePet(petArrary[val].Pet);
				gm.EnableShopHealMachineMenu();
				if(guardar)
					soPj.SpendMoneda(petArrary[val].price);

            });          

        }

		// Cura a todas las pets del protagonista 
        hudCurarComprar.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() =>
        {
            foreach (SOPet pet in soPj.pets){
				if (pet == null)
					break;
                pet.FullHeal();
			}
			gm.EnableShopHealMachineMenu();
		});
    } 
}
