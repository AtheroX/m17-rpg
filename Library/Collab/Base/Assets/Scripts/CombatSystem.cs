﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CombatSystem : MonoBehaviour
{
    public SOPlayer data;
	public SOPet myPet, enPet;
	public GameObject myHud, enHud; 
	public GameObject attacks, attackGO;
	public Slider myHpSlider, enHpSlider;

	public GameObject canvasGO;
	public enum CombatMenu { COMBAT, INVENTORY, PETS }

    enum CombatState { MYTURN, ETURN, ACTION }
    private CombatState actualState;
    int atkSelected;

    void Awake()
    {
		attacks.SetActive(false);
		LoadMyHud();
		LoadEnHud();
		LoadAttacks();
        actualState = CombatState.MYTURN;
		ChangeCombatMenu(CombatMenu.COMBAT);
		InventoryManager(0);
		canvasGO.transform.GetChild(1).GetChild(0).gameObject.GetComponent<ItemDisplayer>().cs = this;
	}

	void Combat()
    {

        if (actualState == CombatState.ETURN) //Selecciona aleatorio o con algoritmo el ataque
        {
			//selAtk(Random.Range(0, enPet.attacks.Length)); //provisional porque dependerá del número de ataques del enemigo
			actualState = CombatState.ACTION;
			Combat();
        } else if (actualState == CombatState.ACTION) {
            //A función, puto. Attack(Pet 
            if (myPet.agl >= enPet.agl) {
                int finalDmg = MyDmgCalc();
				StartCoroutine(Hit(enPet, finalDmg, true));
				finalDmg = EnDmgCalc();
				StartCoroutine(Hit(myPet, finalDmg, false));

			} else if (myPet.agl < enPet.agl) 
                myPet.hp -= EnDmgCalc();
			//else 
			//    if (Random.Range(0, 2) == 0)
			//        enPet.hp -= MyDmgCalc();
			//     else 
			//        myPet.hp -= EnDmgCalc();
        }
	}

	/// <summary>
	/// -1 si mi pet es debil, 0 si son mismo tipo, 1 si es fuerte
	/// </summary>
	/// <returns></returns>
	private int MyDmgCalc()
    {
        int typePet = new CompareTypes().Compare(myPet.attacks[atkSelected].type, enPet.type);

		if (typePet == 1)
            typePet = 2;
        else if(typePet == -1)
            typePet = (int) 0.5f;
        else
            typePet = 1;

        int bonificadorDeDmg = Random.Range(85, 101);
		int lvlvalue = (int)(0.2f * (myPet.lvl + 1) * myPet.attacks[atkSelected].dmg);
		int defvalue = (enPet.def);

		print(typePet + " | " + bonificadorDeDmg + " | " + lvlvalue + " | " + defvalue + " \\ " + (lvlvalue/defvalue));

		return (int)((typePet * bonificadorDeDmg * (lvlvalue / defvalue))/100);
    }

	IEnumerator Hit(SOPet pet, int dmg, bool ihitted) {
		print("muero"+dmg);
		if (ihitted) {
			int anterior = enPet.hp;
			enPet.hp = enPet.hp - dmg < 0 ? 0 : enPet.hp - dmg;
			for (float t = 0; t < 1f; t += Time.deltaTime) {
				enHpSlider.value = Mathf.Lerp(anterior, enPet.hp, t);
				yield return null;
			}
		} else {
			int anterior = myPet.hp;
			myPet.hp = myPet.hp - dmg < 0 ? 0 : myPet.hp - dmg;
			for (float t = 0; t < 1f; t += Time.deltaTime) {
				myHpSlider.value = Mathf.Lerp(anterior, myPet.hp, t);
				yield return null;
			}
		}
	}

    private int EnDmgCalc()
    {
        int typePet = new CompareTypes().Compare(enPet.attacks[atkSelected].type, myPet.type);

        if (typePet == 1)
            typePet = 2;
        else if (typePet == -1)
            typePet = (int)0.5f;
        else
            typePet = 1;

        int bonificadorDeDmg = Random.Range(85, 101);

        return (int)0.01f * typePet * bonificadorDeDmg * ((int)(0.2f * enPet.lvl + 1) / (25 * myPet.def));
    }

    private void LoadMyHud() {
		myHud.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.name.ToUpperInvariant());
		myHud.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.lvl + "");
		myHpSlider = myHud.transform.GetChild(1).GetChild(1).GetComponent<Slider>();
		//myHpSlider.wholeNumbers = true;
		myHpSlider.maxValue = myPet.maxHp;
		myHpSlider.value = myPet.hp;

		Slider xp = myHud.transform.GetChild(2).GetChild(0).GetComponent<Slider>();
		xp.wholeNumbers = true;
		xp.maxValue = myPet.maxXp;
		xp.value = myPet.xp;
	}

	private void LoadEnHud() {
		enHud.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(enPet.name.ToUpperInvariant());
		enHud.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(enPet.lvl + "");
		enHpSlider = enHud.transform.GetChild(1).GetChild(1).GetComponent<Slider>();
		//enHpSlider.wholeNumbers = true;
		enHpSlider.maxValue = enPet.maxHp;
		enHpSlider.value = enPet.hp;
	}

	void LoadAttacks() {
		for (int i = 0; i < myPet.attacks.Length; i++) {
			if (myPet.attacks[i] == null)
				break;
			GameObject o = Instantiate(attackGO, attacks.transform);
			o.SetActive(true);
			o.transform.name = "Attack" + i;
			o.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].name);
			o.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].PP + "/" + myPet.attacks[i].maxPP);
			o.transform.GetChild(1).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(myPet.attacks[i].type.ToString().ToUpper());
			Button b = o.GetComponent<Button>();
			int num = i;
			b.onClick.AddListener(() => { SelAtk(num); });

		}
	}

	void SelAtk(int i)
    {
		print("a" + i);
		atkSelected = i;
		attacks.SetActive(false);
		actualState = CombatState.ETURN;
		Combat();
	}

	public void ChangeCombatMenu(CombatMenu cm) {
		for (int i = 0; i < Enum.GetValues(typeof(CombatMenu)).Length; i++) {
			if ((int)cm == i)
				canvasGO.transform.GetChild(i).gameObject.SetActive(true);
			else
				canvasGO.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	public void ChangeCombatMenu(int cm) {
		for (int i = 0; i < Enum.GetValues(typeof(CombatMenu)).Length; i++) {
			if (cm == i)
				canvasGO.transform.GetChild(i).gameObject.SetActive(true);
			else
				canvasGO.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Cambia dentro del menú de inventory si se ven Pets o items
	/// </summary>
	/// <param name="i">0= Items - 1= Pets</param>
	public void InventoryManager(int i) {
		Transform inventory = canvasGO.transform.GetChild(1);
		if(i == 0) {
			inventory.GetChild(0).gameObject.SetActive(true);
			inventory.GetChild(1).gameObject.SetActive(false);
			
		}else{
			inventory.GetChild(0).gameObject.SetActive(false);
			inventory.GetChild(1).gameObject.SetActive(true);
			

		}
	}
}
