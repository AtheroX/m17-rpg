﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shopPets : MonoBehaviour{

	GameManager gm;
    public SOPlayer soPj;
    GameObject hudPets;
    GameObject hudCurarComprar;
    public GameObject petGO;

	[System.Serializable]
	public class PetTienda {
		public SOPet Pet;
		public int price;
	}
	public PetTienda[] petArrary;
   

    public void Display()
    {
		gm = FindObjectOfType<GameManager>();

		hudPets = gm.HUD.transform.GetChild(2).GetChild(0).gameObject;
        hudCurarComprar = gm.HUD.transform.GetChild(2).GetChild(1).gameObject;

        if (petArrary.Length < 1)
            return;

        for (int i = 0; i < hudPets.transform.childCount; i++)
        {
            Destroy(hudPets.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < petArrary.Length; i++)
        {
            if (petArrary[i] == null)
                break;

            GameObject pet = Instantiate(petGO, hudPets.transform);

            pet.transform.name = petArrary[i].Pet.name;
            pet.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(petArrary[i].Pet.name + " Lvl" + petArrary[i].Pet.lvl);
            pet.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText("" + petArrary[i].Pet.type);
            pet.transform.GetChild(2).GetComponent<Image>().sprite = petArrary[i].Pet.sprite;
            TMPro.TextMeshProUGUI valores = pet.transform.GetChild(3).GetComponent<TMPro.TextMeshProUGUI>();

            valores.SetText(valores.text + "\n HP " + petArrary[i].Pet.hp + " / " + petArrary[i].Pet.maxHp);
            valores.SetText(valores.text + "\n DMG " + petArrary[i].Pet.dmg);
            valores.SetText(valores.text + "\n AGL " + petArrary[i].Pet.agl);
            valores.SetText(valores.text + "\n DEF " + petArrary[i].Pet.def);

            int val = i;

			//Elegir pet
            pet.GetComponent<Button>().onClick.AddListener(() =>
            {
                //hudPets.SetActive(false);
                //hudCurarComprar.SetActive(true);
                soPj.SavePet(petArrary[val].Pet);
				gm.EnableShopHealMachineMenu();
                Comprar(petArrary[val].price);

            });            

        }

		//Curar
        hudCurarComprar.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() =>
        {
            foreach (SOPet pet in soPj.pets){
				if (pet == null)
					break;
                pet.FullHeal();
			}
			gm.EnableShopHealMachineMenu();
		});
    }

    private void Comprar (int val)
    {       
        soPj.SpendMoneda(val);
    }    
}
