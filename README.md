Un Pokémon del Among us, el mejor juego del mundo.

Controles:
- WASD para movimiento
- ESC para Menú
- F Interactuar
- Espacio Girar el Spinner

Risketos Mínims
- Vida (HP)
- Recurs secundari (MP, PP, Stamina…)
- Nivell (i experiència)
- Objectes
    - Objectes d’equip que modifiquen les estadístiques
    - Consumibles
- Estadístiques que afectin al combat
- El jugador ha de conservar-se en canvis d’escenes (persistència)


Risketos Adicionales
- Editores modificados
- Carga asincrona de escena
- 2 Easter eggs (Párticulas de la nave y 2 items)
- Animación de cambio de escena
- Subclases
- Detección de colisiones con un OverlapSphere
